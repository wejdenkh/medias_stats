from flask import Flask, render_template, request, redirect, url_for, session, flash
from pymongo import MongoClient
from bson.objectid import ObjectId


app = Flask(__name__)
app.secret_key = "secret_key"

client = MongoClient("mongodb+srv://wejdenkhessib:tanitweb@cluster0.623dy8y.mongodb.net/?retryWrites=true&w=majority")
db = client["Tanit_Web"]
collection = db["urls"]


@app.route("/", methods=["GET", "POST"])
def list_urls():
    if session.get("logged_in"):
        urls = list(collection.find())
        return render_template("index.html", urls=urls)
    else:
        return redirect(url_for("login"))


@app.route("/add", methods=["GET", "POST"])
def add_url():
    if session.get("logged_in"):
        if request.method == "POST":
            label = request.form["label"]
            url = request.form["url"]
            username = request.form["username"]
            password = request.form["password"]
            status = request.form["status"]
            if collection.find_one({"url": url}):
                return "URL already exists"
            else:
                collection.insert_one({"label": label, "url": url, "username": username, "password": password, "status": status})
                return redirect(url_for("list_urls"))
        else:
            return render_template("add.html")
    else:
        return redirect(url_for("login"))


@app.route("/edit/<url_id>", methods=["GET", "POST"])
def edit_url(url_id):
    if session.get("logged_in"):
        url = collection.find_one({"_id": ObjectId(url_id)})
        if request.method == "POST":
            label = request.form["label"]
            username = request.form["username"]
            password = request.form["password"]
            status = request.form["status"]
            collection.update_one({"_id": ObjectId(url_id)}, {"$set": {"label": label, "username": username, "password": password, "status": status}})
            return redirect(url_for("list_urls"))
        else:
            return render_template("edit.html", url=url)
    else:
        return redirect(url_for("login"))


@app.route("/delete/<url_id>")
def delete_url(url_id):
    if session.get("logged_in"):
        collection.delete_one({"_id": ObjectId(url_id)})
        return redirect(url_for("list_urls"))
    else:
        return redirect(url_for("login"))


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        if username == "admin" and password == "admin":
            session["logged_in"] = True
            return redirect(url_for("list_urls"))
        else:
           return "Invalid login"
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    session["logged_in"] = False
    return redirect(url_for("login"))



if __name__ == '__main__':
    app.run(debug=True)
