import json
from pymongo import MongoClient
from influxdb import InfluxDBClient
import requests
import time

# Connexion à MongoDB
client = MongoClient('mongodb+srv://wejdenkhessib:tanitweb@cluster0.623dy8y.mongodb.net/?retryWrites=true&w=majority')
db = client['Tanit_Web']
collection = db["urls"]

# Connexion à InfluxDB
influx_client = InfluxDBClient('localhost', 8086, 'medias_stats')

# Fonction pour collecter et stocker les données de streaming
def collect_and_store_data(url):
    while True:
        response = requests.get(url)
        data = json.loads(response.content)
        for entry in data:
            # Parse les données pour extraire les champs nécessaires
            fields = {
                'field1': entry['field1'],
                'field2': entry['field2'],
                'field3': entry['field3']
            }
            # Stocke les données dans InfluxDB
            json_body = [
                {
                    'measurement': 'medias_stats',
                    'tags': {
                        'url': 'url'
                    },
                    'time': entry['timestamp'],
                    'fields': fields
                }
            ]
            influx_client.write_points(json_body)
        # Attends 1 seconde avant de collecter les prochaines données
        time.sleep(1)

# Collecte et stocke les données de streaming pour chaque URL stockée dans MongoDB
for url_entry in collection.find():
    url = url_entry['url']
    collect_and_store_data(url)

